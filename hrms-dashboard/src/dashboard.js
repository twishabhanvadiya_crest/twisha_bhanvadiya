import React ,{ Component }from 'react';
import Docs from './DocsAndNotice/DocsAndNotice';
import Events from './Calender/Calender';
import Joinees from './Joinees/Joinees';

// import { Container,Row,Col } from 'react-grid-system'
class dashboard extends Component{
    state ={
        docs:[
          "Your Handbook","Current month payslip","Current project report","Your Goal sheet"
        ],
    announce:[
        {note: "Leave Creadits are updated" ,day: '4th June'},
        {note: "Extention of WFH" ,day: '2nd June'},
        {note: "Reinstall VPN service" ,day: '30th may'}

    ],
    birthDay:[
        {name:"hasti bagadiya" ,day: 'today'},
        {name:"twisha bhanvadiya" ,day: '2 days'},
        {name:"sameer karli" ,day: '5 days'},
        {name:"rohan borkhataria" ,day: '5 days'},
        {name:"mehul jadav" ,day: '7 days'},
    ],
    holidays:[
        {day: 'Rakshabandhan', date:'3rd Aug'},
        {day: 'Gandhi jayanti', date:'2nd Oct'},
        {day: 'kali chaudas', date:'13th Nov'},
        {day: 'Diwali', date:'14th Nov'},

    ],
    joinees:[
        {date: '1st June' ,names:['harshil sadrakiya','dharva mistri']},
        {date:'20th May', names:['devanshu gohel']}
    ]}
    
    render(){
        return(
            <div style={{margin: '1%'}}>

                <div style={{width: '99.5%',
                textAlign: 'left',
                fontSize: '50px',
                color: 'white',
                paddingLeft: '10px',
                borderRadius: '10px',
                backgroundColor: '#263238'}}>
                System dashboard 
                </div>
                <Docs details={this.state.docs} announce={this.state.announce}/>
                <Events birthday={this.state.birthDay} holiday={this.state.holidays}/>
                <Joinees joinees={this.state.joinees}/>
        </div>
        )
    }
   
   
}

export default dashboard;