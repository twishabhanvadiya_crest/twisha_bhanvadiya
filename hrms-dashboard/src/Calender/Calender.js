import React from 'react';
import './Calender.css';

const Events = (props) =>(
    
    <div className="cal">
        <div className="Birthdays" style={{marginTop: '30%'}}>
            <div style={{backgroundColor: '#6095b3', paddingLeft:'5px'}}>Birthdays</div>
            <p style={{marginTop:'10px'}}>{props.birthday.map(birthday => {
                return <div style={{
                    fontSize: '18px',
                    paddingLeft: '5px'
                }}>{birthday.name}<div style={{fontSize:'15px',float: 'right',marginRight:'5px',color:'gray'}}>- {birthday.day}</div><hr/></div>
            })}
            </p>
        </div>
        <div className="Holidays">
            <div style={{backgroundColor: '#6095b3', paddingLeft:'5px'}}>Holidays</div>
            <p style={{marginTop:'10px'}}>{props.holiday.map(holiday => {
                return <div style={{
                    fontSize: '18px',
                    paddingLeft: '5px'
                }}>{holiday.day}<div style={{fontSize:'15px',float: 'right',marginRight:'5px',color:'gray'}}>- {holiday.date}</div><hr/></div>
            })}
            </p>
        </div>
        
    </div>

);

export default Events