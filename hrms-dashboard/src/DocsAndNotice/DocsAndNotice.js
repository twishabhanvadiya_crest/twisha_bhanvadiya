import React from 'react';
import './DocsAndNotics.css';
import ProfileCard from './ProfileCard'


const Docs = (props) =>(
    
    <div className="Profile">
        <ProfileCard/>
        <div className="DocsAndLinks">
            <div style={{backgroundColor: '#6095b3'}}>Docs and Links </div>
            <p style={{marginTop:'10px'}}>{props.details.map(docs => {
                return <div style={{
                    fontSize: '18px',
                    paddingLeft: '5px'
                }}>{docs}<hr/></div>
            })}
            </p>
        </div>
        <div className="Announcemnets">
            <div style={{backgroundColor: '#6095b3'}}>Announcements</div>
            <p style={{marginTop:'10px'}}>{props.announce.map(ann => {
                return <div style={{
                    fontSize: '18px',
                    paddingLeft: '5px'
                }}>{ann.note}<div style={{fontSize:'15px',float: 'right',marginRight:'5px',color:'gray'}}>- {ann.day}</div><hr/></div>
            })}
            </p>
        </div>
    </div>

);

export default Docs