import React from 'react';
import './ProfileCard.css';
import ImgComponent from "react-rounded-image";
import Photo from '../person.png';
import { BsClockHistory } from "react-icons/bs";

const profileCard = () => (
    <div style={{height: "15%"}}>
        <div style={{margin: '25px', float: 'left'}}>
            <ImgComponent 
            image={Photo}
            roundedSize="0"
            imageWidth="70"
            imageHeight="70"/>
        </div>
        <div className="profile">
            <div>Twisha Bhanvadiya</div>
            <div style={{fontSize: 'large'}}>software intern</div>
        </div>
        <div className="leave"> 
            <div style={{float: 'left'}}>
                <p>leave Balance</p>
                <p style={{fontSize:'25px',marginTop:'0'}}>12.5</p>
            </div>
            <div style={{float: 'right',margin: '20px'}}><BsClockHistory size='2.5rem'/></div>
        </div>
    </div>
)

export default profileCard