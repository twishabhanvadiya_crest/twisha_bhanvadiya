import React from 'react';
import './Joinees.css';
import { IoIosPeople } from "react-icons/io";
import ImgComponent from "react-rounded-image";
import Photo from '../person.png';

const Joinees = (props) =>(
    
    <div className="employees">
        <div className="users"> 
            <div style={{float: 'left'}}>
                <p>Number of Users</p>
                <p style={{marginLeft:'40px',fontSize:'25px',marginTop:'0'}}>325</p>
            </div>
            <div style={{float: 'right',margin: '20px'}}><IoIosPeople size='3.5rem'/></div>
        </div>
        <div className="Joinees">
            <div style={{backgroundColor: '#6095b3', paddingLeft:'5px'}}>Crest Welcomes</div>
            <p style={{marginTop:'10px'}}>{props.joinees.map(joine => {
                return <div style={{
                    fontSize: '20px',
                    paddingLeft: '5px',
                    color: 'gray'
                }}>{joine.date} <hr/>
                {joine.names.map(name =>{
                    return <div style={{
                        fontSize: '18px',
                        paddingLeft: '5px',
                        color: 'black'
                }}> 
                <div style={{height:'30px'}}>    
                    <div style={{float:'left'}}>
                    <ImgComponent 
                    image={Photo}
                    roundedSize="0"
                    imageWidth="20"
                    imageHeight="20"
                    />
                    </div>
                    <div style={{float: 'left', marginLeft: '5px'}}>{name}</div>
                </div>
                <hr/>
                </div>
                
            })}
            
            </div>
            })}
            </p>
        
        </div>
    </div>

);

export default Joinees